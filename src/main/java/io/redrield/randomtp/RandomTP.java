package io.redrield.randomtp;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Random;

public class RandomTP extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();
    }

    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("wild") && cs instanceof Player) {
            Player p = (Player) cs;
            Random r = new Random();
            int y = p.getWorld().getSeaLevel();
            int x = r.nextInt(5000)+1;
            int z = r.nextInt(5000)+1;
            Location random = new Location(p.getWorld(), (double)x, (double)y, (double)z);
            Material bType;
            while((bType = p.getWorld().getBlockAt(random).getType())!=Material.AIR) {
                random.add(0, 1, 0);
            }
            p.teleport(random);
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("teleport_message")));
            return true;
        }
        return false;
    }
}
